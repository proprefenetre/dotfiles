# -*- Conf -*-

# Key bindings

# definitions
set $super Mod4
set $alt Mod1
set $ctrl Control
set $hyper Mod3

set $left d
set $down h
set $up t
set $right n

# set $term gnome-terminal --hide-menubar
set $term alacritty

# set $menu dmenu_run -fn 'Hack Nerd Font 8' -l 5
set $menu dmenu_run

# basics
bindsym $super+Return exec $term
bindsym $super+Shift+Return exec emacs28

# Kill focused window
bindsym $super+c kill

# Start your launcher
bindsym $super+space exec $menu

# Use Mouse+$super to drag floating windows to their wanted position
floating_modifier $super

# focus container 
bindsym $super+$left focus left
bindsym $super+$down focus down
bindsym $super+$up focus up
bindsym $super+$right focus right

# move container 
bindsym $super+Shift+$left move left
bindsym $super+Shift+$down move down
bindsym $super+Shift+$up move up
bindsym $super+Shift+$right move right

# Move focused container to workspace
bindsym $super+Shift+1 move container to workspace number 1
bindsym $super+Shift+2 move container to workspace number 2
bindsym $super+Shift+3 move container to workspace number 3
bindsym $super+Shift+4 move container to workspace number 4
bindsym $super+Shift+5 move container to workspace number 5

# Workspaces:
workspace_auto_back_and_forth yes
bindsym $super+apostrophe workspace next_on_output
bindsym $super+Shift+apostrophe workspace prev_on_output

# Switch to workspace
bindsym $super+1 workspace number 1
bindsym $super+2 workspace number 2
bindsym $super+3 workspace number 3
bindsym $super+4 workspace number 4
bindsym $super+5 workspace number 5

# Layout stuff:

# You can "split" the current object of your focus with
# $super+b or $mod+v, for horizontal and vertical splits
# respectively.
bindsym $super+b splith
bindsym $super+v splitv

# Switch the current container between different layout styles
bindsym $super+m layout toggle tabbed split

# Make the current focus fullscreen
bindsym $super+f fullscreen
bindsym $super+Shift+f focus parent; fullscreen; focus child

for_window [class="^scratch$"] floating enable
for_window [class="^scratch$"] move scratchpad
bindsym $hyper+u scratchpad show

mode "layout" {
    bindsym h splith; mode "default"   
    bindsym v splitv; mode "default" 
    bindsym f floating toggle; mode "default"

    bindsym t layout toggle split; mode "default"
    bindsym m layout tabbed; mode "default"

    bindsym o move workspace to output left; mode "default"
    bindsym Shift+o move workspace to output downup; mode "default"

    bindsym Escape mode "default"
}
bindsym $super+period mode "layout"


mode "focus" {
    bindsym f focus mode_toggle; mode "default"
    bindsym p focus parent; mode "default"
    bindsym c focus child; mode "default"

    bindsym Escape mode "default"
}
bindsym $super+comma mode "focus"

mode "resize" {
    bindsym $left resize shrink width 10px
    bindsym $down resize grow height 10px
    bindsym $up resize shrink height 10px
    bindsym $right resize grow width 10px

    bindsym Left resize shrink width 10px
    bindsym Down resize grow height 10px
    bindsym Up resize shrink height 10px
    bindsym Right resize grow width 10px

    # Return to default mode
    bindsym Return mode "default"
    bindsym Escape mode "default"
}
bindsym $super+Shift+r mode "resize"

set $meta "[E]xit, [r]eload"
mode $meta {
    # bindsym $super+Shift+e exec swaynag -t warning -m 'You pressed the exit shortcut. Do you really want to exit sway? This will end your Wayland session.' -b 'Yes, exit sway' 'swaymsg exit'

    bindsym o move workspace to output left; mode "default"
    bindsym Shift+e exec swaymsg exit
    bindsym r reload; mode "default"

    bindsym Escape mode "default"
    bindsym Return mode "default"

    # bindsym l exec --no-startup-id ~/bin/lock, mode "default"
    # bindsym k exec --no-startup-id i3-msg exit, mode "default"
    # bindsym t exec --no-startup-id i3-msg restart; mode "default"
    # bindsym e exec $editor ~/.config/i3/config; mode "default"
    # bindsym r exec --no-startup-id i3-msg reload; mode "default"

    # bindsym Escape mode "default"
    # bindsym Return mode "default"
}
bindsym $super+r mode $meta

# ===
# Font for window titles. Will also be used by the bar
# unless a different font is used in the bar {} block below.
# font pango: Hack Regular 6
font pango: Dejavu Sans 7

hide_edge_borders both
new_window pixel 1
default_orientation horizontal

set $ws1 workspace 1
set $ws2 workspace 2
set $ws3 workspace 3
set $ws4 workspace 4
set $ws5 workspace 5

# launching
set $browser firefox
set $editor vim

# Start i3bar to display a workspace bar (plus the system information i3status
# finds out, if available)
bar {
    status_command i3blocks
    position top
    font pango:Hack Regular, awesome 8
    colors {
        background  #1c1c1c
        focused_workspace #1c1c1c #1c1c1c #ffffff
        inactive_workspace #333333 #222222 #888888
        urgent_workspace   #712B31 #5D0D14 #ffffff

    }
}

# colors
# class                 border  backgr. text    indicator child_border
# client.focused          #0c8346 #0d5d56 #ffffff #2e9ef4   #0d3c5d
client.focused          #5f676a #ffffff #ffffff #2e9ef4   #ffffff
client.focused_inactive #333333 #5f676a #ffffff #484e50   #0d3c5d
client.unfocused        #333333 #222222 #888888 #292d2e   #222222
client.urgent           #2f343a #900000 #ffffff #900000   #900000
client.placeholder      #000000 #0c0c0c #ffffff #000000   #0c0c0c

# Autostart
exec xmodmap ~/.Xmodmap
# exec --no-startup-id $term --class scratch --hide-menubar
exec --no-startup-id $term --class scratch 
