-- neovim config - lua mode 
local set = vim.opt

set.number = true
set.showmatch = true
set.splitright = true
set.gdefault = true
set.autowriteall = true

set.smartcase = true
set.ignorecase = true
set.grepprg = 'rg --vimgrep --follow'

set.completeopt = {'menu', 'menuone', 'noselect'}

set.scrolloff = 2
set.tabstop = 4
set.shiftwidth = 4
set.softtabstop = 4
set.expandtab = true

set.statusline = "%M%<%f [%(%R, %)%Y, %{(&fenc!=''?&fenc:&enc)}] %=%-14.(%l,%c%V%) %P "
