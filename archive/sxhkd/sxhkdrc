# -*- mode: conf -*-

# Chord chains: multiple chords separated by a semicolon. A colon can be used
# to indicate that the chord chain should stay active (e.g. for resizing)
# If a command starts with a semicolon, it will be executed synchronously,
# otherwise asynchronously. The Escape key can be used to abort a chord chain.

# @[keysym]: command will be run on key release events
# ~[keysym]: captured events will be replayed for the other clients. (what does that even mean?)
# _:  an empty sequence element.

# mod1 = alt
# mod3 = hyper
# mod4 = super
#
# wm independent hotkeys
#


## Super
#
# terminal emulator
super + Return
    kitty

# emacsclient
super + shift + Return
     emax

# program launcher
super + @space
     dmenu_run -fn 'Hack 12' -l 5
 
# MAKE SXHKD reload its configuration files:
alt + Escape
    pkill -USR1 -x sxhkd && echo "$(date +'%d-%m-%Y %H:%M:%S') - reload config" >> ~/.config/sxhkd/log

# hide window
super + a
    bspwm-hide-window.sh

## Mouse buttons
# button3 + shift + control
button6 + shift
    trackball-precision-mode.sh

# screenshot
Print
     bspwm-screenshots.sh


## Hyper 

# browser
mod3 + 1
    firefox-developer-edition

mod3 + 2
    emax

mod3 + l
    lock

mod1 + space
    keeb -t


mod3 + t
    toggle-touchpad.sh

# scratchterm
mod3 + u
    bspwm-scratchterm.sh


## window stuff 

# close and kill
super + {_,shift + }c
    bspc node -{c,k}

# alternate between the tiled and monocle layout
super + m
    bspc desktop -l next

# swap the current node and the biggest node
super + g
    bspc node -s biggest.local.window

## rotate 90 deg
super + {_,shift} + r
    bspc node @parent -R {90,270}

#
# state/flags
#

# set window mode
super + s ; {t,shift + t}
    bspc node -t {tiled,floating}

# toggle fullscreen
super + f
    bspc node -t ~fullscreen

# set the node flags
super + ctrl + {m,l,s,p}
    bspc node -g {marked,locked,sticky,private}

#
# focus/swap
#

# focus the node in the given direction
super + {d,h,t,n}
    bspc node --focus {west,south,north,east}

# swap current node with the node in the given direction
super + shift + {d,h,t,n}
    bspc node --swap {west,south,north,east}

# focus the next/previous node in the current desktop
super + {_,shift + }Tab
    bspc node -f {next,prev}.local.!hidden.window

# focus the next/previous node in the current desktop
super + {period,comma}
    bspc desktop -f {next,prev}

# focus the older or newer node in the focus history
super + {o,i}
    bspc wm -h off; \
    bspc node {older,newer} -f; \
    bspc wm -h on

# focus or send to the given desktop
super + {_,shift + }{1-9,0}
    bspc {desktop -f,node -d} '^{1-9,10}'

super + {_,shift + }apostrophe
    bspc desktop -f {next,prev}
   
# send to monitor
super + shift + apostrophe ; {1,2}
     bspc node -m ^{1,2}

## focus other monitor
# super + apostrophe
#      bspc monitor -f last

#
# preselect
#

# preselect the direction
super + ctrl + {d,h,t,n}
    bspc node -p {west,south,north,east}

# preselect the ratio
super + ctrl + {1-9}
    bspc node -o 0.{1-9}

# cancel the preselection for the focused node
super + ctrl + space
    bspc node -p cancel

# cancel the preselection for the focused desktop
super + ctrl + shift + space
    bspc query -N -d | xargs -I id -n 1 bspc node id -p cancel

#
# move/resize
#


# send the newest marked node to the newest preselected node
super + y
    bspc node newest.marked.local -n newest.!automatic.local

# expand a window by moving one of its side outward
alt + super + {d,h,t,n}
    bspc node -z {left -20 0,bottom 0 20,top 0 -20,right 20 0}

# contract a window by moving one of its side inward
alt + super + shift + {d,h,t,n}
    bspc node -z {right -20 0,top 0 20,bottom 0 -20,left 20 0}

# move a floating window
super + {Left,Down,Up,Right}
    bspc node -v {-20 0,0 20,0 -20,20 0}

# center
super + semicolon; {a,o,e,u,shift + a,shift + o,shift + e,shift + u,semicolon}
     bspwm-corner.sh -{ul,ll,ur,lr,u,d,l,r,c}

# volume 
{XF86AudioRaiseVolume,XF86AudioLowerVolume,XF86AudioMute}
    amixer set Master {1%+,1%-,toggle}

{XF86MonBrightnessUp,XF86MonBrightnessDown}
    xbacklight {+,-} 10
