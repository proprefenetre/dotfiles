#! /usr/bin/env python

from pathlib import Path
from pprint import pprint
import re

SYMBOL = """
partial modifier_keys
xkb_symbols "rctl" {
    key <RCTL> { [ Hyper_R ] };
    modifier_map Mod3 { <HYPR>, Hyper_R };
};
"""

EVDEV_RULE = "  hyper:rctl    =     +hyper(rctl)"

EVDEV_LST_RULE = """  hyper		       Hyper key
  hyper:rctl	       Right Ctrl als Hyper"""

EVDEV_XML_RULE = """    <group allowMultipleSelection="true">
      <configItem>
        <name>hyper</name>
        <description>Position of the Hyper key</description>
      </configItem>
      <option>
        <configItem>
          <name>hyper:rctl</name>
          <description>Right Ctrl as Hyper</description>
        </configItem>
      </option>
    </group>"""


if __name__ == "__main__":

    rule_file = Path("/usr/share/X11/xkb/symbols/hyper")
    evdev_f = Path("/usr/share/X11/xkb/rules/evdev")
    evdev_lst_f = Path("/usr/share/X11/xkb/rules/evdev.lst")
    evdev_xml_f = Path("/usr/share/X11/xkb/rules/evdev.xml")

    if not rule_file.exists():
        with rule_file.open("w", encoding="utf-8") as f:
            f.write(SYMBOL)

    # evdev.xml
    with evdev_xml_f.open("r") as f:
        print(f"Editing {evdev_xml_f.resolve()}")
        cont = f.read()

    lines = cont.split("\n")
    for idx, line in enumerate(lines):
        end = re.match(r"\s+</optionList>", line)
        if end:
            section = EVDEV_XML_RULE.split("\n")
            lines = lines[:idx] + section + lines[idx:]
            break

    with evdev_xml_f.open("w") as f:
        f.write("\n".join(lines))

    # evdev.lst: just append the line
    with evdev_lst_f.open("a") as f:
        print(f"Editing {evdev_lst_f.resolve()}")
        f.write(EVDEV_LST_RULE)

    # evdev
    with evdev_f.open("r") as f:
        print(f"Editing {evdev_f.resolve()}")
        cont = f.read()

    lines = cont.split("\n")
    for idx, line in enumerate(lines):
        end = re.match(r"\s+parens:swap_brackets", line)
        if end:
            lines = lines[:idx+1] + [EVDEV_RULE] + lines[idx+1:]
            break

    with evdev_f.open("w") as f:
        f.write("\n".join(lines))
