;;; -*- lexical-binding: t; -*-
;;; Commentary:
;;; init.el --- Emacs config, longer by the day
;;; Code:
(let ((default-directory "~/.dotfiles/emacs.d/"))
  (normal-top-level-add-subdirs-to-load-path))

(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
      (bootstrap-version 6))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

(use-package straight
  :custom
  ;; add project and flymake to the pseudo-packages variable so straight.el doesn't download a separate version than what eglot downloads.
  (straight-built-in-pseudo-packages '(emacs nadvice python image-mode project flymake org))
  (straight-use-package-by-default t))

(straight-use-package 'use-package)

(use-package exec-path-from-shell
  :demand t
  :config
  (exec-path-from-shell-initialize))

(use-package no-littering
  :demand t
  :config
  (setq auto-save-file-name-transforms `((".*" ,(no-littering-expand-var-file-name "auto-save/") t)))
  (setq backup-directory-alist `(("." . ,(no-littering-expand-var-file-name "backups/"))))
  (setq custom-file (no-littering-expand-etc-file-name "custom.el")))

(load custom-file)

(add-to-list 'load-path (expand-file-name "config" user-emacs-directory))

(require 'pfn-org)
(require 'pfn-evil)
(require 'pfn-completion)
(require 'pfn-keys)
(require 'pfn-programming)
(require 'pfn-functions)
(require 'pfn-hydras)

(use-package persistent-scratch
  :config
  (persistent-scratch-setup-default)
  (persistent-scratch-mode))

(use-package nerd-icons)

(use-package doom-themes
  :init
  (setq custom-safe-themes t)
  (load-theme 'doom-tomorrow-night t)
  :config
  (setq doom-one-brighter-comments nil)
  (doom-themes-org-config))

(use-package doom-modeline
  :hook (after-init . doom-modeline-mode)
  :config
  ;; (setq nerd-icons-font-family "Hack Nerd Font Mono"
  (setq nerd-icons-font-family "Iosevka Nerd Font"
	column-number-mode t
	doom-modeline-icon t
	doom-modeline-buffer-file-name-style 'relative-from-project
	doom-modeline-check-simple-format t)
  )

(use-package solaire-mode
  :config
  (solaire-global-mode 1))

(use-package magit
  :demand t)

(use-package shackle
  :demand t
  :config
  (setq shackle-rules '(("*Python*" :noselect t :size .25 :align 'below)
			("COMMIT_EDITMSG" :select t :align 'right)
			(magit-status-mode :same t :inhibit-window-quit t)
			("CAPTURE.*\\.org" :regexp t :same t)
			("*Help*" :select t :align below :size .25)
			("*org-roam*" :select t)
			("\\*Org Src .*\\*" :regex t :same t)
			))
  (shackle-mode 1))

(use-package rainbow-delimiters
  :demand t
  :hook ((prog-mode text-mode) . rainbow-delimiters-mode)
  :config
  (set-face-attribute 'rainbow-delimiters-unmatched-face nil
                      :foreground 'unspecified
                      :background 'unspecified
                      :inverse-video nil
                      :weight 'normal))


;; Use puni-mode globally and disable it for term-mode.
(use-package puni
  :init
  ;; The autoloads of Puni are set up so you can enable `puni-mode` or
  ;; `puni-global-mode` before `puni` is actually loaded. Only after you press
  ;; any key that calls Puni commands, it's loaded.
  (puni-global-mode)
  (add-hook 'term-mode-hook #'puni-disable-puni-mode))

(use-package aggressive-indent
  :demand t
  :config
  (dolist (mode '(text-mode html-mode python-ts-mode dockerfile-ts-mode rust-ts-mode))
    (add-to-list 'aggressive-indent-excluded-modes mode))
  (global-aggressive-indent-mode))

(use-package projectile
  :demand t
  :config
  (setq projectile-sort-order 'recently-active)
  (projectile-mode))

(use-package symbol-overlay
  :demand t
  :config
  (setq symbol-overlay-displayed-window t))

(use-package indent-bars
  :straight (indent-bars :type git :host github :repo "jdtsmith/indent-bars")
  :hook ((python-ts-mode yaml-ts-mode) . indent-bars-mode)
  :custom
  (indent-bars-treesit-support t)
  (indent-bars-no-descend-string t)
  (indent-bars-treesit-ignore-blank-lines-types '("module"))
  (indent-bars-treesit-wrap '((python argument_list parameters ; for python, as an example
				      list list_comprehension
				      dictionary dictionary_comprehension
				      parenthesized_expression subscript))
			    )
  (indent-bars-width-frac 0.2)
  :config
  (setq indent-bars-prefer-character t))

(use-package highlight-numbers
  :hook ((prog-mode) . highlight-numbers-mode))

(use-package highlight-escape-sequences
  :hook ((prog-mode text-mode) . hes-mode))

(use-package ace-window
  :demand t
  :config
  (setq aw-scope 'frame))

(use-package olivetti
  :hook (org-mode . olivetti-mode)
  :init
  (setq olivetti-body-width .80
	olivetti-style nil))

(use-package undo-tree
  :init
  (global-undo-tree-mode)
  :config
  (setq undo-tree-auto-save-history nil))

(use-package dirvish
  :config
  (setq dirvish-use-header-line 'global)    ; make header line span all panes
  ;; Go back home? Just press `bh'
  (setq dirvish-quick-access-entries
	'(
	  ("c" "~/.dotfiles/emacs.d/coonfig" "configs")
	  ("d" "~/downloads" "downloads")
	  ("e" "~/.dotfiles/emacs.d/" "emacs.d")
	  ("f" "~/files" "files")
	  ("h" "~/"      "Home")
	  ("o" "~/files/org" "org")
	  ("p" "~/projects" "projects")
	  ("r" "~/files/roam" "org-roam")
	  ))
  ;; Dired options are respected except a few exceptions, see *In relation to Dired* section above
  (setq dirvish-mode-line-format '(:left (sort file-time " " file-size symlink) :right (omit yank index)))
  (setq dirvish-attributes '(vc-state subtree-state nerd-icons collapse git-msg file-time file-size))
  (setq dired-dwim-target t)
  (setq dired-listing-switches "-l --almost-all --human-readable --time-style=long-iso --group-directories-first --no-group")
  (dirvish-override-dired-mode))

(use-package treemacs
  :config
  (treemacs-follow-mode t)
  (treemacs-git-mode 'deferred)
  (treemacs-indent-guide-mode 'line))

(use-package treemacs-evil
  :after treemacs)

(use-package treemacs-projectile
  :after treemacs)

(use-package treemacs-magit
  :after treemacs)

(use-package treemacs-tab-bar
  :after treemacs)

(use-package editorconfig
  :ensure t
  :config
  (editorconfig-mode 1))

;; (use-package dogears
;;   :config
;;   (dogears-mode))

(use-package burly
  :config
  (burly-tabs-mode))

(use-package popper
  :config
  (popper-mode))

(use-package vterm)

(use-package deadgrep)

(use-package expand-region)

(use-package dashboard
  :init
  (setq dashboard-set-heading-icons t)
  :config
  (setq dashboard-startup-banner 'logo)
  (setq dashboard-center-content t)
  (setq dashboard-icon-type 'nerd-icons) 
  (setq dashboard-agenda-prefix-format "%-12:c %s ")

  (setq dashboard-items '((recents   . 5)
                          (bookmarks . 5)
                          (agenda    . 5)))

  (setq dashboard-startupify-list '(dashboard-insert-banner
                                    dashboard-insert-newline
                                    dashboard-insert-banner-title
                                    dashboard-insert-newline
                                    dashboard-insert-navigator
                                    dashboard-insert-newline
                                    dashboard-insert-init-info
                                    dashboard-insert-items
                                    dashboard-insert-newline))
  (dashboard-setup-startup-hook))

(use-package annotate
  :config
  (annotate-mode))

(use-package rainbow-mode)

(use-package tab-bar
  :straight (:type built-in)
  :config
  (setq tab-bar-new-tab-choice "*dashboard*")
  (setq tab-bar-tab-name-function #'(lambda ()
				      (if (string= "-" (projectile-project-name))
					  (tab-bar-tab-name-current)
					(projectile-project-name))))
  (tab-bar-mode 1))

(use-package pdf-tools)

(use-package jinx
  :init
  (setq jinx-languages "en_GB nl_NL"))

(use-package ligature
  :config
  ;; Enable all Iosevka ligatures in programming modes
  (ligature-set-ligatures '(prog-mode text-mode) '("<---" "<--"  "<<-" "<-" "->" "-->" "--->" "<->" "<-->" "<--->" "<---->" "<!--"
						   "<==" "<===" "<=" "=>" "=>>" "==>" "===>" ">=" "<=>" "<==>" "<===>" "<====>" "<!---"
						   "<~~" "<~" "~>" "~~>" "::" ":::" "==" "!=" "===" "!=="
						   ":=" ":-" ":+" "<*" "<*>" "*>" "<|" "<|>" "|>" "+:" "-:" "=:" "<******>" "++" "+++"))
  ;; Enables ligature checks globally in all buffers. You can also do it
  ;; per mode with `ligature-mode'.
  (global-ligature-mode t))

(use-package sideline
  :init
  (setq sideline-backends-left-skip-current-line t   ; don't display on current line (left)
        sideline-backends-right-skip-current-line nil  ; don't display on current line (right)
        sideline-order-left 'up                    ; or 'up
        sideline-order-right 'down                     ; or 'down
        sideline-format-left "%s   "                 ; format for left aligment
        sideline-format-right "   %s"                ; format for right aligment
        sideline-priority 100                        ; overlays' priority
        sideline-display-backend-name nil))            ; display the backend nam

(use-package sideline-eglot
  :straight (:host github :repo "emacs-sideline/sideline-eglot")
  :init
  (setq sideline-backends-right '(sideline-eglot)))

(use-package sideline-flymake
  :hook (flymake-mode . sideline-mode)
  :init
  (setq sideline-flymake-display-mode 'point) ; 'point to show errors only on point 'line to show errors on the current line
  (setq sideline-backends-right '(sideline-flymake)))

(use-package blamer
  :ensure t
  :bind (("s-i" . blamer-show-commit-info)
         ("C-c i" . blamer-show-posframe-commit-info))
  :defer 20
  :custom
  (blamer-idle-time 0.3)
  (blamer-min-offset 70)
  :custom-face
  (blamer-face ((t :foreground "#7a88cf"
                   :background nil
                   :height 140
                   :italic t)))
  )

(use-package bicycle
  :after outline)

(use-package emacs
  :init

  (setq completion-styles '(orderless basic))
  (setq tab-always-indent 'complete)

  (add-to-list 'magic-mode-alist '("---" . yaml-mode))

  (fset 'yes-or-no-p 'y-or-n-p)

  (set-language-environment "UTF-8")
  (set-default-coding-systems 'utf-8)

  ;; (set-face-attribute 'default nil :font "Iosevka Nerd Font Mono 13")
  (set-face-attribute 'default nil :font "Hack Nerd Font 13")

  (setq browse-url-firefox-program "firefox-developer-edition")

  (setq default-input-method "latin-postfix"
	initial-scratch-message ""
	indent-tabs-mode nil
	tab-width 4
	scroll-margin 10
	scroll-conservatively most-positive-fixnum
	confirm-kill-emacs 'yes-or-no-p
	use-short-answers t
	x-select-enable-clipboard t
	vc-follow-symlinks t
	display-line-numbers-width 4
	display-line-numbers-width-start 3
	display-line-numbers-widen nil
	bookmark-save-flag 1
	bookmark-default-file (no-littering-expand-var-file-name "bookmarks")
	TeX-engine 'xelatex
	latex-run-command "xelatex"
	tramp-default-method "ssh"
	save-abbrevs 'silent
	desktop-restore-frames nil
	inhibit-startup-screen t
	browse-url-browser-function 'browse-url-firefox
	require-final-newline t
	ring-bell-function 'ignore
	create-lockfiles nil
	warning-minimum-level :error)

  (dolist (table abbrev-table-name-list)
    (abbrev-table-put (symbol-value table) :case-fixed t))

  (put 'downcase-region 'disabled nil)              ; Enable downcase-region
  (put 'upcase-region 'disabled nil)                ; Enable upcase-region
  (put 'narrow-to-region 'disabled nil)             ; Enable narrowing

  (add-hook 'focus-out-hook 'garbage-collect)
  (add-hook 'find-file-hook 'recentf-save-list)

  (abbrev-mode 1)
  (auto-fill-mode -1)
  (global-auto-revert-mode)
  (global-eldoc-mode)
  (recentf-mode 1)
  (savehist-mode 1)
  ;; prog-mode hooks

  (dolist (hook '(hs-minor-mode
		  outline-minor-mode
		  display-line-numbers-mode
		  hl-line-mode
		  electric-pair-mode))
    (add-hook 'prog-mode-hook hook))

  ;; text-mode hooks
  (dolist (hook '(visual-line-mode
		  electric-pair-mode
		  rainbow-delimiters-mode))
    (add-hook 'text-mode-hook hook))

  ;; Do not allow the cursor in the minibuffer prompt
  (setq minibuffer-prompt-properties
	'(read-only t cursor-intangible t face minibuffer-prompt))
  (add-hook 'minibuffer-setup-hook #'cursor-intangible-mode)

  ;; Emacs 28: Hide commands in M-x which do not work in the current mode.
  ;; Vertico commands are hidden in normal buffers.
  (setq read-extended-command-predicate
	#'command-completion-default-include-p)

  (setq enable-recursive-minibuffers t)
  (setq auto-save-timeout 10)
  )
;;; Init.el ends here
