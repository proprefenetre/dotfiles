;;; -*- lexical-binding: t; -*-
;;; pfn-programming.el --- programming setup
;;; Commentary:
;;; Code:

;; (use-package eldoc-box
;;   :config
;;   (eldoc-box-hover-at-point-mode))

(defun pfn-eglot-workspace-configuration (server)
  '((:pylsp . (:plugins (:ruff (:enabled t) :mypy (:enabled t :live_mode t :strict nil))))))

(use-package eglot
  :straight (:type built-in)
  :hook ((eglot-managed-mode . tempel-setup-capf))
  :config
  ;; enable cache-busting for eglot
  (advice-add 'eglot-completion-at-point :around #'cape-wrap-buster)
  ;; (add-hook 'eglot-managed-mode-hook #'tempel-setup-capf)
  (setq eglot-events-buffer-size 0)
  (setq eglot-workspace-configuration #'pfn-eglot-workspace-configuration)
  (add-to-list 'eglot-server-programs
               '((rust-ts-mode rustic-mode) .
		 ("rust-analyzer" :initializationOptions (:check (:command "clippy") :diagnostics (:enable :json-false)))))
  )

;; needed for emacs29
;; (use-package eglot-booster
;;   :straight (:host github :repo "jdtsmith/eglot-booster")
;;   :after eglot
;;   :config
;;   (eglot-booster-mode))

(use-package treesit
  :straight (:type built-in)
  :init
  (setq treesit-language-source-alist
	'((bash "https://github.com/tree-sitter/tree-sitter-bash")
	  (elisp "https://github.com/Wilfred/tree-sitter-elisp")
	  (html "https://github.com/tree-sitter/tree-sitter-html")
	  (json "https://github.com/tree-sitter/tree-sitter-json")
	  (markdown "https://github.com/ikatyang/tree-sitter-markdown")
	  (python "https://github.com/tree-sitter/tree-sitter-python")
	  (rust "https://github.com/tree-sitter/tree-sitter-rust")
	  (toml "https://github.com/tree-sitter/tree-sitter-toml")
	  (tsx "https://github.com/tree-sitter/tree-sitter-typescript" "master" "tsx/src")
	  (typescript "https://github.com/tree-sitter/tree-sitter-typescript" "master" "typescript/src")
	  (yaml "https://github.com/ikatyang/tree-sitter-yaml")
	  (dockerfile "https://github.com/camdencheek/tree-sitter-dockerfile" "main")
	  (php "https://github.com/tree-sitter/tree-sitter-php")
	  (java "https://github.com/tree-sitter/tree-sitter-java")
	  (lua "https://github.com/tjdevries/tree-sitter-lua")
	  )
	)

  ;; (mapc #'treesit-install-language-grammar (mapcar #'car treesit-language-source-alist))

  (setq major-mode-remap-alist
	'((yaml-mode . yaml-ts-mode)
	  (bash-mode . bash-ts-mode)
	  (js2-mode . js-ts-mode)
	  (typescript-mode . typescript-ts-mode)
	  (js-json-mode . json-ts-mode)
	  (python-mode . python-ts-mode)
	  (rust-mode . rust-ts-mode)
	  (toml-mode . toml-ts-mode)))

  (customize-set-variable 'treesit-font-lock-level 4))

(use-package pyvenv)

(use-package pet
  :config
  (add-hook 'python-ts-mode-hook 'pet-mode -10))

;; (add-hook 'window-selection-change-functions 'pfn-activate-venv-if-python-buffer)
;; (add-hook 'projectile-after-switch-project-hook 'pfn-activate-venv-if-python-buffer)

(defun pfn-python-setup-treesitter ()
  (interactive)
  (message "recomputing font lock features")
  (treesit-font-lock-recompute-features '() '(function operator variable)))

(use-package python-mode
  :straight (:type built-in)
  :hook ((python-ts-mode . pfn-python-setup-treesitter))
  :config
  (setq python-shell-interpreter "ipython")
  (setq python-shell-interpreter-args "--simple-prompt -i")
  (setq python-indent-offset 4))

(use-package ruff-format
  :hook (python-ts-mode . ruff-format-on-save-mode))

;; Rust
(defun pfn-rust-setup-treesitter ()
  ;; ((comment definition)
  ;; (keyword string)
  ;; (assignment attribute builtin constant escape-sequence number type)
  ;; (bracket delimiter error function operator property variable))
  (interactive)
  (message "recomputing font lock features")
  (treesit-font-lock-recompute-features '() '(property function)))

(use-package rust-mode)
(use-package rust-ts-mode
  :mode "\\.rs\\'"
  :hook ((rust-ts-mode . pfn-rust-setup-treesitter)
	 (rust-ts-mode . eglot-ensure))
  )

(use-package rustic
  :after org
  :config
  (setq rustic-lsp-client 'eglot))

(use-package dockerfile-mode
  :mode ("Dockerfile\\'" . dockerfile-ts-mode))

(use-package yaml-ts-mode
  :straight (:type built-in)
  :mode ("\\.yml\\'" "\\.yaml\\'" "\\.yml.j2\\'")
  :config
  (add-hook 'yaml-ts-mode-hook #'(lambda () (display-line-numbers-mode)))
  (hl-line-mode 1))

(use-package toml-ts-mode
  :straight (:type built-in)
  :mode ("\\.toml\\'" "\\.lock\\'")
  :config
  (add-hook 'toml-ts-mode-hook #'(lambda () (display-line-numbers-mode)))
  (add-hook 'toml-ts-mode-hook #'(lambda () (rainbow-delimiters-mode)))
  (hl-line-mode 1))

(use-package yaml-pro
  :hook (yaml-ts-mode . yaml-pro-mode))

(use-package java-ts-mode
  :straight (:type built-in))

(use-package eglot-java
  :hook (java-ts-mode . eglot-java-mode))

(use-package bash-ts-mode
  :straight (:type built-in)
  :hook (bash-ts-mode . eglot-ensure))

(use-package fish-mode)
(use-package php-mode)
(use-package go-mode)
(use-package groovy-mode)
(use-package lua-mode)
(provide 'pfn-programming)

;;; pfn-programming.el ends here
