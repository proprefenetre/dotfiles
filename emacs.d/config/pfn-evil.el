;;; pfn-evil.el --- evil setup
;;; Commentary:
;;; Code:
(use-package evil
  :demand t
  :init
  (setq evil-want-keybinding nil
        evil-want-integration t
        evil-want-Y-yank-to-eol t
        evil-want-C-w-delete t)
  :config
  (eval-after-load 'evil-ex
    '(evil-ex-define-cmd "w" nil))
  (setq evil-insert-state-modes nil
        evil-motion-state-modes nil
        evil-search-wrap t
        evil-regexp-search t
        evil-complete-next-func 'hippie-expand
        evil-vsplit-window-right t
        evil-split-window-below t
        evil-cross-lines t
        evil-ex-substitute-global t)
  (evil-set-undo-system 'undo-tree)
  (evil-select-search-module 'evil-search-module 'evil-search)
  ;; (evil-set-initial-state 'pdf-view-mode 'emacs)
  (add-hook 'pdf-view-mode-hook
	    (lambda ()
	      (set (make-local-variable 'evil-normal-state-cursor) (list nil))))
  (evil-mode 1))

(use-package evil-collection
  :after evil
  :demand t
  :init
  (setq evil-want-keybinding nil)
  (setq evil-collection-outline-bind-tab-p nil
        evil-collection-setup-minibuffer nil)
  :config
  (evil-collection-init))

(use-package evil-commentary
  :after evil
  :demand t
  :config (evil-commentary-mode))

(use-package evil-surround
  :after evil
  :demand t
  :config
  (global-evil-surround-mode))

(use-package evil-org
  :after (evil org)
  :demand t
  :hook (org-mode . evil-org-mode)
  :config
  (setq evil-org-set-key-theme '(textobjects insert navigation)))

(use-package evil-easymotion
  :after evil
  :demand t
  :config
  (evilem-default-keybindings "SPC"))

(use-package evil-goggles
  :config
  (evil-goggles-mode)

  ;; optionally use diff-mode's faces; as a result, deleted text
  ;; will be highlighed with `diff-removed` face which is typically
  ;; some red color (as defined by the color theme)
  ;; other faces such as `diff-added` will be used for other actions
  (evil-goggles-use-diff-faces))

(use-package evil-owl
  :config
  (setq evil-owl-max-string-length 500)
  ;; (add-to-list 'display-buffer-alist
  ;;              '("*evil-owl*"
  ;;                (display-buffer-in-side-window)
  ;;                (side . bottom)
  ;;                (window-height . 0.3)))
  (setq evil-owl-display-method 'posframe
        evil-owl-extra-posframe-args '(:width 50 :height 20 :border-width 1 :border-color "#4271ae" :internal-border-width 1 :internal-border-width "#ffffff")
        evil-owl-max-string-length 50)
  (evil-owl-mode))

(provide 'pfn-evil)
;;; pfn-evil.el ends here
