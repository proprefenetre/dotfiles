;;; pfn-keys.el --- bindings using General.el
;;; Commentary:
;;; Code:
;;; -*- lexical-binding: t; -*-
(use-package which-key
  :demand t
  :config (which-key-mode 1))

(setq tab-always-indent 'complete)

(use-package general
  :demand t
  :config
  (general-evil-setup)
  (general-override-mode)
  (general-auto-unbind-keys)

  (general-create-definer evil-leader
    :prefix ",")

  (evil-leader
    :states '(normal visual emacs treemacs)
    :keymaps 'override
    "b" 'consult-buffer
    "c" 'capitalize-dwim
    "d" 'dired-jump
    "f" 'consult-flymake
    "g" 'evil-commentary-yank-line
    "i" '(lambda () (interactive)
           (find-file user-init-file))
    "n" 'symbol-overlay-rename
    "o" 'olivetti-mode
    "p" 'consult-yank-pop
    "q" 'delete-window
    "r" '(lambda () (interactive)
           (revert-buffer :ignore-auto :noconfirm))
    "s" 'magit-status
    "t" 'treemacs-select-window
    "u" 'tempel-insert)

  (general-def
    :prefix "C-c"
    "c"   '(lambda () (interactive)
             (org-capture nil "c"))
    "C-d" 'dirvish
    "f"   'flymake-hydra/body
    "l"   'org-store-link
    "C-l" 'comint-clear-buffer
    ;; "p"   'projectile-command-map
    "p p" 'consult-projectile-switch-project
    "p f" 'consult-projectile-find-file
    "p F" 'consult-projectile-find-file-other-window
    "p k" 'projectile-kill-buffers
    "r b" 'org-roam-buffer-toggle
    "r i" 'org-roam-node-insert
    "r f" 'org-roam-node-find
    "r c" 'org-roam-capture
    "r d" 'org-roam-dailies-capture-today
    "r t" 'org-roam-dailies-goto-today
    "R"   '(lambda () (interactive) (revert-buffer :ignore-auto :noconfirm))
    "s"   'consult-ripgrep
    "e o" 'eglot-code-action-organize-imports
    "e a" 'eglot-code-actions
    "e r" 'eglot-rename)

  (general-def
    :prefix "C-x"
    "ESC ESC" 'keyboard-quit
    "b"       'ibuffer
    "r b"     'consult-bookmark
    "f"       'consult-recent-file
    "k"       'kill-current-buffer
    "2"       '(lambda () (interactive)
		 (split-window-below)
		 (other-window 1))
    "3"       '(lambda () (interactive)
		 (split-window-right)
		 (other-window 1))
    )

  (general-def
    "<menu>" 'execute-extended-command
    "M-x"    'execute-extended-command
    "M-/"    'hippie-expand
    "C-)"    'puni-slurp-forward
    "C-("    'puni-slurp-backward
    "C-}"    'puni-barf-forward
    "C-{"    'puni-barf-backward
    "C-s"    'consult-line
    "C-l"    'evil-ex-nohighlight
    "M-'"    'popper-toggle
    "M-\""    'popper-toggle-type
    "C->"    'popper-cycle
    "C-t"    'treemacs-select-window
    )

  (general-mmap
    "j"   'evil-next-visual-line
    "k"   'evil-previous-visual-line
    "C-e" 'evil-end-of-line
    "C-b" 'mode-line-other-buffer)

  (general-nmap
    ;; :states 'normal
    "gt"       'tab-bar-switch-to-next-tab
    "gT"       'tab-bar-switch-to-prev-tab
    )

  (general-vmap
    ")" 'er/expand-region
    "(" 'er/contract-region)

  (general-def goto-map
    "f" 'avy-goto-char
    "t" 'avy-goto-word-1
    "g" 'dumb-jump-hydra/body
    "p" 'python-hydra/body
    "d"    'dogears-go
    "M-b"  'dogears-back
    "M-f"  'dogears-forward
    "M-d"  'dogears-list
    "M-D"  'dogears-sidebar)

  (general-def evil-window-map
    "C-w" 'ace-window
    "C-s" 'dirvish-side
    "C-t" 'treemacs
    "v" 'evil-window-vnew 
    "s" 'evil-window-split) 

  (general-def rust-mode-map
    "C-c <tab>" 'rust-format-buffer)

  (general-def org-mode-map 
    :states '(insert normal) 
    "<return>" 'evil-org-return)

  (general-def 
    :states 'normal
    :keymaps '(dired-mode-map dirvish-mode-map)
    "q"   'dirvish-quit
    "b"   'dirvish-quick-access
    "f"   'dirvish-file-info-menu
    "y"   'dirvish-yank-menu
    "N"   'dirvish-narrow
    "^"   'dirvish-history-last
    "h"   'dirvish-history-jump ; remapped `describe-mode'
    "s"   'dirvish-quicksort    ; remapped `dired-sort-toggle-or-edit'
    "TAB" 'dirvish-subtree-toggle
    "M-n" 'dirvish-history-go-forward
    "M-p" 'dirvish-history-go-backward
    "M-l" 'dirvish-ls-switches-menu
    "M-m" 'dirvish-mark-menu
    "M-f" 'dirvish-layout-toggle
    "M-e" 'dirvish-emerge-menu
    "M-j" 'dirvish-fd-jump)

  (general-def
    :states 'normal
    :keymaps 'clojure-mode-map
    ")"   'puni-forward-sexp
    "("   'puni-backward-sexp
    "C-d" 'puni-kill-line
    )

  (general-def persistent-scratch-mode-map
    "C-x C-s" 'persistent-scratch-save)

  (general-def
    :states 'insert
    :keymaps 'tempel-map
    "C-n" 'tempel-next
    "C-p" 'tempel-previous)
  )

(provide 'pfn-keys)
;;; pfn-keys.el ends here
