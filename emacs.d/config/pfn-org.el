;;; -*- lexical-binding: t; -*-
;;; pfn-org.el --- setup org-mode
;;; Commentary:
;;; Code:

(defun pfn-electric-pair-skip-pointy-bracket (char)
  (if (char-equal char ?<) t
    (electric-pair-default-inhibit char)))

(use-package org
  :straight (:type built-in)
  :hook ((org-mode . (lambda ()
		       (setq-local electric-pair-inhibit-predicate 'pfn-electric-pair-skip-pointy-bracket)))
	 (org-mode . (lambda () (setq-local tab-with 8))))
  :config

  (setq org-directory "~/files/org"
        org-cycle-separator-lines 2
        org-blank-before-new-entry '((heading . t)
                                     (plain-list-item . nil))
        org-return-follows-link t
        org-reverse-note-order t
        org-outline-path-complete-in-steps t
        org-log-done nil
        org-startup-indented t
	org-hide-leading-stars t
        org-startup-folded t
        org-pretty-entities t
	org-use-sub-superscripts nil
	org-level-color-stars-only nil
	org-edit-src-content-indentation 0)

  (setq org-todo-keywords '((sequence "TODO" "ERROR(e)" "AFWACHTEN(w)" "BEZIG(b)" "|" "DONE" "WONTDO(c)")))

  ;; use theme colors
  (setq org-todo-keyword-faces
        '(("TODO" . "#ff6655")
          ("BEZIG" . "#dd8844")
          ("AFWACHTEN" . "#dd8844" )
          ("WONTDO" . "#BF616A")
          ("ERROR" . "#BF616A")
          ("DONE" . "#99bb66")
          ))

  (org-babel-do-load-languages
   'org-babel-load-languages
   '((python . t)
     (shell . t)))
  )

(use-package org-roam
  :after org
  :init
  (setq org-roam-directory (file-truename "~/files/roam"))
  :config
  (org-roam-db-autosync-enable))

(provide 'pfn-org)
;;; pfn-org.el ends here
