;;; pfn-hydras.el
;;; -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

(use-package hydra)

(defhydra python-hydra ()
  "python"
  ("a" pfn-auto-activate-venv "auto-activate")
  ("A" pyvenv-activate "activate")
  ("e" eglot "eglot" :color blue))

(provide 'pfn-hydras)
;;; pfn-hydras ends here
