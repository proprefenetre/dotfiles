;;-*-coding: utf-8;-*-
(define-abbrev-table 'org-mode-abbrev-table
  '(
    ("aif" "Airflow" nil :count 5)
    ("api" "API" nil :count 8)
    ("cls" "cluster" nil :count 10)
    ("cnt" "container" nil :count 6)
    ("deng" "data engineer" nil :count 5)
    ("dsc" "data science" nil :count 4)
    ("dwh" "data warehouse" nil :count 2)
    ("een'" "één" nil :count 8)
    ("elas" "Elasticsearch" nil :count 2)
    ("json" "JSON" nil :count 1)
    ("k8s" "Kubernetes" nil :count 10)
    ("mbv" "met behulp van" nil :count 5)
    ("minio" "MinIO" nil :count 2)
    ("nifi" "NiFi" nil :count 22)
    ("obv" "op basis van" nil :count 3)
    ("sfs" "StatefulSet" nil :count 8)
    ("zookeeper" "ZooKeeper" nil :count 11)
   ))

