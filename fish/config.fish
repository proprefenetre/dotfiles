if status is-interactive
    # Commands to run in interactive sessions can go here
    if status --is-interactive
        keychain --eval ~/.ssh/id_rsa | source
    end

    atuin init fish --disable-up-arrow | source

    set -g fish_greeting
    set -ga fish_function_path $fisher_path

    # rust
    set -g RUST_SRC_PATH (rustc --print sysroot)/lib/rustlib/src/rust/src

    # fzf
    set -g FZF_DEFAULT_COMMAND 'fd --type f'

    # Wayland
    set -g MOZ_ENABLE_WAYLAND 1

    # Poetry
    set -g PYTHON_KEYRING_BACKEND keyring.backends.null.Keyring

    # intellij
    set -g _JAVA_AWT_WM_NONREPARENTING 1
    set -g AWT_TOOLKIT MToolkit

    set -g EDITOR nvim

    fish_add_path ~/bin ~/.cargo/bin ~/.local/bin ~/.poetry/bin

end

abbr -a -- gst 'git status'
abbr -a -- ga 'git add'
abbr -a -- gp 'git push'
abbr -a -- gf 'git pull'
abbr -a -- gs 'git status'

abbr -a -- pr 'poetry run'
abbr -a -- prp 'poetry run python'
abbr -a -- prt 'poetry run pytest'

