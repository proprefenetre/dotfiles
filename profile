#! /usr/bin/env bash

[[ -f ~/.bashrc ]] && . ~/.bashrc

export TERM="xterm-256color"

export DOTFILES="$HOME/.dotfiles"
export KEEPASS_DB="$HOME/files/secret_stuff.kdbx"
export HISTSIZE=50000
export HISTFILESIZE=50000
export PROMPT_COMMAND='history -a'
export CM_SELECTIONS=clipboard
export EDITOR='vim'
export ALTERNATE_EDITOR=""
export BROWSER='firefox-developer-edition'
export LESS='-R'
export LESSOPEN='| /usr/bin/source-highlight-esc.sh %s'

export PATH="${PATH}:$HOME/bin:$HOME/.cargo/bin:$HOME/.local/bin:$HOME/.poetry/bin"
# export PATH="${PATH}:$HOME/bin:$HOME/.cargo/bin:$HOME/.local/bin"

# rust
export RUST_SRC_PATH="$(rustc --print sysroot)/lib/rustlib/src/rust/src"

# fzf
export FZF_DEFAULT_COMMAND='fd --type f'

# Wayland
export MOZ_ENABLE_WAYLAND=1

# Poetry
export PYTHON_KEYRING_BACKEND="keyring.backends.null.Keyring"

# intellij
export _JAVA_AWT_WM_NONREPARENTING=1
export AWT_TOOLKIT=MToolkit
