#! /usr/bin/env bash

if [[ -z "$DOTFILES" ]]; then
    echo "\$DOTFILES not set"
    exit 1
fi

for file in bash_aliases bashrc profile emacs.d ssh tmux.conf; do
    ln -sn "$DOTFILES/$file" "$HOME/.$file"
done

for file in flake8 kitty alacritty black gammastep spotifyd sway waybar; do
    ln -sn "$DOTFILES/$file" "$HOME/.config/$file"
done
