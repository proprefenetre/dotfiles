-- Folding
-- vim.cmd([[ set foldlevelstart=99]])
--
-- vim.api.nvim_create_autocmd('FileType', {
--   callback = function()
--     if require("nvim-treesitter.parsers").has_parser() then
--       vim.opt.foldmethod = "expr"
--       vim.opt.foldexpr = "nvim_treesitter#foldexpr()"
--     else
--       vim.opt.foldmethod = "syntax"
--     end
--   end,
-- })

-- Highlight when yanking (copying) text
vim.api.nvim_create_autocmd('TextYankPost', {
  desc = 'Highlight when yanking (copying) text',
  callback = function()
    vim.highlight.on_yank()
  end,
})
