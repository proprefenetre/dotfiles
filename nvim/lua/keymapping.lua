local function cmd(command)
  return "<Cmd>" .. command .. "<CR>"
end

vim.g.mapleader = ','
vim.g.maplocalleader = ','

-- vim.keymap.set('n', '<leader>i', '<cmd>e $MYVIMRC<CR>')
vim.keymap.set('n', '<leader>i', cmd("e $MYVIMRC"))

-- Clear highlights on search when pressing <Esc> in normal mode
--  See `:help hlsearch`
vim.keymap.set('n', '<Esc>', cmd('nohlsearch'))

-- Diagnostic keymaps
vim.keymap.set('n', '<leader>q', cmd('q'))

--  See `:help wincmd` for a list of all window commands
vim.keymap.set('n', '<C-h>', '<C-w><C-h>', { desc = 'Move focus to the left window' })
vim.keymap.set('n', '<C-l>', '<C-w><C-l>', { desc = 'Move focus to the right window' })
vim.keymap.set('n', '<C-j>', '<C-w><C-j>', { desc = 'Move focus to the lower window' })
vim.keymap.set('n', '<C-k>', '<C-w><C-k>', { desc = 'Move focus to the upper window' })

vim.keymap.set("n", "<C-f>", require('fzf-lua').files, { desc = "Fzf Files" })
vim.keymap.set("n", "<C-b>", require('fzf-lua').buffers, { desc = "Fzf Files" })

vim.keymap.set('n', "<leader>B", "<cmd>Grapple toggle<cr>", { desc = "Grapple toggle tag" })
vim.keymap.set('n', "<leader>b", "<cmd>Grapple toggle_tags<cr>", { desc = "Grapple open tags window" })
vim.keymap.set('n', "<leader>n", "<cmd>Grapple cycle_tags next<cr>", { desc = "Grapple cycle next tag" })
vim.keymap.set('n', "<leader>p", "<cmd>Grapple cycle_tags prev<cr>", { desc = "Grapple cycle previous tag" })

vim.api.nvim_create_autocmd('LspAttach', {
  callback = function(args)
    local client = vim.lsp.get_client_by_id(args.data.client_id)
    if client then
      if client.server_capabilities.hoverProvider then
        vim.keymap.set('n', 'K', vim.lsp.buf.hover, { buffer = args.buf })
      end
      if client.server_capabilities.signatureHelpProvider then
        vim.keymap.set({ 'n', 'i' }, '<C-k>', vim.lsp.buf.signature_help, { buffer = args.buf })
      end
      if client.server_capabilities.declarationProvider then
        vim.keymap.set('n', 'gD', vim.lsp.buf.declaration, { buffer = args.buf })
      end
      if client.server_capabilities.definitionProvider then
        vim.keymap.set('n', 'gd', vim.lsp.buf.definition, { buffer = args.buf })
      end
      if client.server_capabilities.typeDefinitionProvider then
        vim.keymap.set('n', 'gy', vim.lsp.buf.type_definition, { buffer = args.buf })
      end
      if client.server_capabilities.implementationProvider then
        vim.keymap.set('n', 'gi', vim.lsp.buf.implementation, { buffer = args.buf })
      end
      if client.server_capabilities.referencesProvider then
        vim.keymap.set('n', 'gr', vim.lsp.buf.references, { buffer = args.buf })
      end
      if client.server_capabilities.renameProvider then
        vim.keymap.set('n', '<F2>', vim.lsp.buf.rename, { buffer = args.buf })
      end
      if client.server_capabilities.codeActionProvider then
        vim.keymap.set('n', '<F4>', vim.lsp.buf.code_action, { buffer = args.buf })
      end

      if client.supports_method(vim.lsp.protocol.Methods.textDocument_documentHighlight) then
        vim.api.nvim_create_autocmd({ 'CursorHold', 'CursorHoldI' }, {
          buffer = args.buf,
          callback = vim.lsp.buf.document_highlight,
        })

        vim.api.nvim_create_autocmd({ 'CursorMoved', 'CursorMovedI' }, {
          buffer = args.buf,
          callback = vim.lsp.buf.clear_references,
        })

        vim.api.nvim_create_autocmd('LspDetach', {
          callback = function()
            vim.lsp.buf.clear_references()
          end,
        })
      end
    end

    vim.keymap.set('n', ']d', vim.diagnostic.goto_next, { buffer = args.buf })
    vim.keymap.set('n', '[d', vim.diagnostic.goto_prev, { buffer = args.buf })
    vim.keymap.set('n', '<space>', vim.diagnostic.open_float, { buffer = args.buf })
    vim.api.nvim_create_user_command('Dllist', vim.diagnostic.setloclist, {})
    vim.api.nvim_create_user_command('Dclist', vim.diagnostic.setqflist, {})
  end,


})
